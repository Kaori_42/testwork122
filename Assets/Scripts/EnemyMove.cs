using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMove : MonoBehaviour
{
    Rigidbody EnemyRigidbody;
    public float speed = 2f;
   
    Transform EnemyTransform;
    int[] rotation = { 90, -90, 180 };


    void Start()
    {   
        EnemyRigidbody = this.GetComponent<Rigidbody>();
        EnemyTransform = this.gameObject.GetComponent<Transform>();
        GameController.NewWave.AddListener(newWave);
        
    }
    private void Update()
    {
        Debug.Log( "speed: " + speed);
    }
    void FixedUpdate()
    {
        Ray ForwardRay = new Ray(new Vector3(EnemyTransform.position.x , EnemyTransform.position.y+1, EnemyTransform.position.z), EnemyTransform.forward*2);
        Debug.DrawRay(new Vector3(transform.position.x, transform.position.y + 0.5f, transform.position.z), transform.forward * 4, Color.white);
       
        EnemyTransform.Translate(Vector3.forward * Time.deltaTime); 

        RaycastHit hit;
        if (Physics.Raycast(ForwardRay , out hit))
        {
            if (hit.distance < 1)
            {
                EnemyTransform.Rotate(new Vector3(0, rotation[Random.Range(0, rotation.Length)] ));
            }
            
        }

    }
    void newWave(bool v)
    {
        speed += (speed * 0.4f);
    }
   
}

