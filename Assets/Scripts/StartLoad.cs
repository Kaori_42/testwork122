using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Events;

public class StartLoad : MonoBehaviour
{
    public TextMeshProUGUI Last, Record;

    int LastVa, RecordVa;

    private void Start()
    {
        Time.timeScale = 1;
        if (PlayerPrefs.HasKey("LastScore"))
        {
            LastVa = PlayerPrefs.GetInt("LastScore");
            Last.text = LastVa.ToString();

        }
        else
        {
            PlayerPrefs.SetInt("LastScore", 0);
            LastVa = 0;

            Last.text = LastVa.ToString();
        }

        if (PlayerPrefs.HasKey("RecordScore"))
        {
            RecordVa = PlayerPrefs.GetInt("RecordScore");
            Record.text = RecordVa.ToString();

        }
        else
        {
            PlayerPrefs.SetInt("RecordScore", 0);
            RecordVa = 0;

            Record.text = RecordVa.ToString();
        }
    }

    
}
