using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Events;
using TMPro;

public class ClickReciew : MonoBehaviour, IPointerClickHandler
{
    int Click = 0, needClick = 1, waveCount = 0, counter = 2;
    public ParticleSystem particle;
    public TextMesh TDtext;

    private void Start()
    {
        TextUpdater(true);
        ObjectSpawner.EnemySpawned.AddListener(TextUpdater);
        GameController.NewWave.AddListener(newWave);
    }
    public void OnPointerClick(PointerEventData eventData)
    {
        Click++;
        TextUpdater(true);
        particle.Play(true);
        if (Click == needClick)
        {
            Click = 0;
            eventData.pointerClick.gameObject.SetActive(false);
            SendEnemyKilled(1);

        }
        
        
    }
    public void newWave(bool v)
    {
        counter++;
        if(waveCount%3 == 0)
        {
            needClick = Random.Range(1, counter) ;
        }
    }

    public static UnityEvent<int> EnemyKilled = new UnityEvent<int>();

    public static void SendEnemyKilled(int score)
    {
        EnemyKilled.Invoke(score);
    }

    public void TextUpdater(bool bd)
    {
        TDtext.text = (needClick - Click).ToString();
    }

}

