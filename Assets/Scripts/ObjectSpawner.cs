using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ObjectSpawner : MonoBehaviour
{
    public List<GameObject> PrefabsToSpawn = new List<GameObject>();
    public List<GameObject> SpawnedObjects = new List<GameObject>();
    public GameObject ParentObject;
    int[] RandomRotation = { 0,90,180,-90,-180 };
    bool spawn = false;
    float spawnPeriod = 1f , SpawnPeriodStart = 0.9f , SpawnPeriodEnd = 1f;

    public int CountOfEachOb = 12;

    private void Start()
    {
        GameController.NewWave.AddListener(NewWave);
        for (int i = 0; i < PrefabsToSpawn.Count; i++)
        {
            for (int j = 0; j < CountOfEachOb; j++)
            {
                var TempGameOb = Instantiate(PrefabsToSpawn[i] , ParentObject.transform );
                SpawnedObjects.Add(TempGameOb);
                TempGameOb.SetActive(false);
            } 
        }

        StartCoroutine(Spawner());
    }
    private void Update()
    {
        Debug.Log(SpawnPeriodStart + " " + SpawnPeriodEnd);
    }
    public void NewWave(bool b)
    {
        SpawnPeriodEnd -= (SpawnPeriodEnd * 0.1f);
        SpawnPeriodStart -= (SpawnPeriodStart * 0.1f);
    }

    public static UnityEvent<bool> EnemySpawned = new UnityEvent<bool>();

    public static void SendEnemySpawned(bool v)
    {
        EnemySpawned.Invoke(v);
    }


    public void GameStart()
    {
        spawn = true;
    }

    public void BoostKill()
    {
        for (int i = 0; i < SpawnedObjects.Count; i++)
        {
            SpawnedObjects[i].SetActive(false);
        }
    }

    public void TimeOut()
    {
        StartCoroutine(TimeOit());
    }
    IEnumerator TimeOit()
    {
        spawn = false;
        yield return new WaitForSeconds(3f);
        spawn = true;
    }
    void SpawnFromPool()
    {
        if (spawn)
        {
            GameObject TempGameOb = null;

            for (int i = 0; i < SpawnedObjects.Count; i++)
            {
                if (SpawnedObjects[i].gameObject.activeSelf == false && SpawnedObjects[i].gameObject!= null)
                {
                    TempGameOb = SpawnedObjects[i];
                    TempGameOb.SetActive(true);
                    break;
                }
            }
            Vector3 NewPosition = new Vector3(Random.Range(-4, 5), 0, Random.Range(1, 10));

            TempGameOb.transform.position = NewPosition;
            TempGameOb.transform.eulerAngles = new Vector3(0, RandomRotation[Random.Range(0, 5)], 0);
            SendEnemySpawned(true);
        }
        



    }

    IEnumerator Spawner()
    {
        
            while (true)
            {
                
                SpawnFromPool();
            spawnPeriod = Random.Range(SpawnPeriodStart, SpawnPeriodEnd);
                yield return new WaitForSeconds(spawnPeriod);
            }
            
        
    }


}
 