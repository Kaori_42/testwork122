using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UiUpdater : MonoBehaviour
{
    public TextMeshProUGUI ScoreText, LoseText;
    public GameObject LosePanel , MainPanel;
    int score = 0;
    int record, last;
    void Start()
    {
        ClickReciew.EnemyKilled.AddListener(ScoreUpdater);
        GameController.GameOver.AddListener(UpdateParametrs);
        ScoreText.text = score.ToString();
    }

    public void ScoreUpdater(int i)
    {
        score += i;
        ScoreText.text = score.ToString();
    }

    public void UpdateParametrs(bool b)
    {
        record = PlayerPrefs.GetInt("RecordScore");

        PlayerPrefs.SetInt("LastScore", score);

        if(score > record)
        {
            PlayerPrefs.SetInt("RecordScore", score);
        }
        LoseText.text = score.ToString();
        MainPanel.SetActive(false);
        LosePanel.SetActive(true);
        Time.timeScale = 0;
    }
}
