using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GameStarter : MonoBehaviour
{
    public List<GameObject> _ObjectsThatHideAfterStart = new List<GameObject>();
    public List<GameObject> _ObjectsThatShowAfterStart = new List<GameObject>();
    public GameObject _ButtonsObject, _CanvasObject;
    Animator ButtonsAnimator, CanvasAnimator;

    private void Start()
    {
        ButtonsAnimator = _ButtonsObject.GetComponent<Animator>();
        CanvasAnimator = _CanvasObject.GetComponent<Animator>();

    }
    public void AnimationsStarter()
    {
        ButtonsAnimator.SetTrigger("Starter");
        CanvasAnimator.SetTrigger("Starter");
       
        StartCoroutine(Disenableter());

    }
    
    IEnumerator Disenableter()
    {
        yield return new WaitForSeconds(0.25f);

        for(int i = 0; i <_ObjectsThatHideAfterStart.Count; i++)
        {
            _ObjectsThatHideAfterStart[i].SetActive(false);
        }

        for (int i = 0; i < _ObjectsThatShowAfterStart.Count; i++)
        {
            _ObjectsThatShowAfterStart[i].SetActive(true);
        }
    }
}
