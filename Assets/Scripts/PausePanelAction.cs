using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PausePanelAction : MonoBehaviour
{
    public Animator CanvasAnimator;
    public GameObject PausePanel;
    public void OpenPause()
    {
        PausePanel.SetActive(true);
        CanvasAnimator.SetTrigger("Open Pause");
        StartCoroutine(PauseOpenDelay());
       
    } 
    public void ClosePause()
    {
        Time.timeScale = 1;
        CanvasAnimator.SetTrigger("Close Pause");
        PausePanel.SetActive(false);
    }
    IEnumerator PauseOpenDelay()
    {
        yield return new WaitForSeconds(0.4f);
        Time.timeScale = 0;
    }
    public void RestartScene()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
    


}
;