using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GameController : MonoBehaviour
{
    int ActiveEnemeCount = 0;
    int killedEnemyCount = 0;

    private void Start()
    {
        ObjectSpawner.EnemySpawned.AddListener(EnemySpawned);
        ClickReciew.EnemyKilled.AddListener(EnemyKilled);
    }

    public void EnemySpawned(bool b)
    {
        ActiveEnemeCount++;
        if (ActiveEnemeCount == 10)
        {
            Lose();
        }
    }
    public void EnemyKilled(int a)
    {
        ActiveEnemeCount--;
        killedEnemyCount++;
        if(killedEnemyCount%10 == 0)
        {
            SendNewWave(true);
        }
    }

    public void MakeNull()
    {
        ActiveEnemeCount = 0;
    }

    public static UnityEvent<bool> NewWave = new UnityEvent<bool>();

    public static void SendNewWave(bool v)
    {
        NewWave.Invoke(v);
    }
    public void Lose()
    {
       
        SendGameFinished(true);
    }

    public static UnityEvent<bool> GameOver = new UnityEvent<bool>();

    public static void SendGameFinished(bool v)
    {
        GameOver.Invoke(v);
    }
}